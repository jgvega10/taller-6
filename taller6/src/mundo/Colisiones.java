package mundo;
import java.util.*;

import taller.estructuras.NodoHash;
import taller.estructuras.TablaHash;

public class Colisiones {

	private TablaHash<String, Ciudadano> tablaPorNombre;
	private TablaHash<String, Ciudadano> tablaPorGPS;
	private TablaHash<String, Ciudadano> tablaPorTelefono;
	private Scanner scan;

	public Colisiones() {
		tablaPorNombre = new TablaHash<>();
		tablaPorGPS = new TablaHash<>();
		tablaPorTelefono = new TablaHash<>();
		scan = new Scanner(System.in);
	}

	public void agregar(Ciudadano persona)
	{
		tablaPorNombre.put(persona.getNombre(), persona);
		
		tablaPorGPS.put(persona.getGPS().darLatitud()+","+persona.getGPS().darLongitud(), persona);
		
		tablaPorTelefono.put(persona.getTelefono(), persona);
	}

	public NodoHash<String, Ciudadano> buscarPorNombre(String nombre) 
	{
		return tablaPorNombre.get(nombre);
	}

	public NodoHash<String, Ciudadano> buscarPorGPS(GPS gps) 
	{
		return tablaPorGPS.get(gps.darLatitud()+","+gps.darLongitud());
	}

	public NodoHash<String, Ciudadano> buscarPorTelefono(String telefono) {
		return tablaPorTelefono.get(telefono);
	}

	public void eliminarPorNombre(String nombre) 
	{
		NodoHash<String, Ciudadano> actual = buscarPorNombre(nombre);

		if(actual != null) 
		{
			tablaPorTelefono.delete(actual.getValor().getTelefono());
			
			tablaPorGPS.delete(actual.getValor().getGPS().darLatitud()+","+actual.getValor().getGPS().darLongitud());
			
			tablaPorNombre.delete(nombre);
		}
	}

	public void eliminarPorTelefono(String telefono) 
	{
		NodoHash<String, Ciudadano> current = buscarPorTelefono(telefono);

		if(current != null)
		{
			tablaPorGPS.delete(current.getValor().getGPS().darLatitud()+","+current.getValor().getGPS().darLongitud());
			
			tablaPorNombre.delete(current.getValor().getNombre());
			
			tablaPorTelefono.delete(telefono);		
		}
	}

	public void eliminarPorPosicion(GPS gps)
	{
		NodoHash<String, Ciudadano> current = buscarPorGPS(gps);

		if(current != null)
		{
			tablaPorTelefono.delete(current.getValor().getTelefono());
			tablaPorNombre.delete(current.getValor().getNombre());
			tablaPorGPS.delete(gps.darLatitud()+","+gps.darLongitud());	
		}
	}

	public void agregarCiudadano()
	{ 
		try {
			
			System.out.println("Por favor ingrese el nombre de la persona: ");
			String nombre = scan.nextLine();
			System.out.println("Ingrese el apellido de la persona: ");
			String apellido = scan.nextLine();
			System.out.println("Ingrese el telefono de la persona: ");
			String telefono = scan.nextLine();
			System.out.println("Ingrese la latitud de la persona (Formato: ###.###): ");
			double latitud = Double.parseDouble(scan.nextLine());
			System.out.println("Ingrese la longitud de la persona (Formato: ###.###): ");
			double longitud = Double.parseDouble(scan.nextLine());
			agregar(new Ciudadano(nombre, apellido, telefono, latitud, longitud));	
		} 
		catch(Exception e)
		{
			System.out.println("No es un formato valido");
		}
	}

	public void buscarCiudadanoPorTelefono() 
	{
		System.out.println("Ingrese el telefono del ciudadano para buscar: ");
		String telefono = scan.nextLine();
		NodoHash<String, Ciudadano> buscada = buscarPorTelefono(telefono);
		mostrarInfoDelCiudadano(buscada);
	}

	public void buscarCiudadanoPorNombre()
	{
		System.out.println("Ingrese el nombre del ciudadano que quiere buscar: ");
		String nombre = scan.nextLine();
		NodoHash<String, Ciudadano> buscada = buscarPorNombre(nombre);
		mostrarInfoDelCiudadano(buscada);
	}

	public void buscarCiudadanoPorPosicion() 
	{
		System.out.println("Por favor ingrese la posicion del ciudadano que quiere buscar (Formato: ###.###,###.###): ");
		String posicion = scan.nextLine();

		try {
			String[] datos = posicion.split(",");
			double lat = Double.parseDouble(datos[0]);
			double lon = Double.parseDouble(datos[1]);
			GPS gps = new GPS(lat,lon);

			NodoHash<String, Ciudadano> buscada = buscarPorGPS(gps);
			
			mostrarInfoDelCiudadano(buscada);

		} 
		catch(Exception e) 
		{
			System.out.println("No ingreso un formato valido");
		}
	}

	public void mostrarInfoDelCiudadano(NodoHash<String, Ciudadano> ciuda)
	{
		System.out.println("*************************************");
		if(ciuda == null)
		{
			System.out.println("El ciudadano no se encontro");
		} 
		else
		{
			for(NodoHash<String, Ciudadano> current = ciuda; current != null; current = current.getSiguiente())
			{
				Ciudadano ciudadano = current.getValor();
				System.out.println("Nombre: " +ciudadano.getNombre());
				System.out.println("Apellido: " +ciudadano.getApellido());
				System.out.println("Telefono: " +ciudadano.getTelefono());
				GPS gps = ciudadano.getGPS();
				System.out.println("Posicion(Lat, Lon): " +gps.darLatitud() +"," +gps.darLongitud());
				System.out.println("***************************************");
			}			
		}
	}
}
