
package mundo;


public class Ciudadano 
{
	private String nombre;
	private String apellido;
	private String num;
	private GPS gps;

	public Ciudadano(String nombre, String apellido, String telefono, String latitud, String longitud) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.num = telefono;
		this.gps = convertirAGPS(latitud, longitud);
	}

	public Ciudadano(String nombre, String apellido, String telefono, double latitud, double longitud) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.num = telefono;
		gps = new GPS(latitud, longitud);
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getTelefono() {
		return num;
	}

	public GPS getGPS() {
		return gps;
	}

	public GPS convertirAGPS(String latitud, String longitud) {		
		GPS res = new GPS(Double.parseDouble(latitud.replace(',', '.')), Double.parseDouble(longitud.replace(',', '.')));

		return res;
	}
	
}
