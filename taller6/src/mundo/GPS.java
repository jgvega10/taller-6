package mundo;

public class GPS implements Comparable<GPS> {

	private double lat;
	private double longi;
	
	public GPS(double latitud, double longitud) 
	{
		this.lat = latitud;
		this.longi = longitud;
	}
	
	public double darLatitud() 
	{
		return lat;
	}
	
	public double darLongitud()
	{
		return longi;
	}
	
	public int compareTo(GPS o) 
	{
		if(this.lat == o.darLatitud()) {
			return 0;
		} else if(this.lat < o.darLatitud()) {
			return -1;
		} else {
			return 1;
		}
	}
}
