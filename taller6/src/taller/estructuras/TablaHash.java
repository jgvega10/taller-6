package taller.estructuras;


public class TablaHash<K extends Comparable<K> ,V> 
{

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Estructura que soporta la tabla.
	 */
	private SecuentialGral<K, V> est[];
	
	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;


	public TablaHash(){
		this(997);
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int pCapacidad) {
		this.count = pCapacidad;
		est = (SecuentialGral<K, V>[]) new SecuentialGral[count];
		
		for(int i = 0; i < count; i++) {
			est[i] = new SecuentialGral<K,V>();
		}
	}

	public void put(K llave, V valor){
		est[hash(llave)].put(llave, valor);
	}

	public NodoHash<K,V> get(K llave){
		return est[hash(llave)].get(llave);
	}
	
	public void delete(K llave) {
		est[hash(llave)].delete(llave);
	}

	private int hash(K llave)
	{
		return (llave.hashCode() & 0x7fffffff) % count;
	}

}