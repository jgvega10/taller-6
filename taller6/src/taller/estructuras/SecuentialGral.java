package taller.estructuras;

public class SecuentialGral <Key extends Comparable<Key>, Value> {

	private NodoHash<Key,Value> first;

	public void put(Key k, Value v) {
		first = new NodoHash<Key,Value>(k, v, first);
	}

	public NodoHash<Key,Value> get(Key k) {
		NodoHash<Key,Value> newList = null;
		
		for(NodoHash<Key,Value> current = first; current != null; current = current.getSiguiente()) {
			if(current.getLlave().equals(k)) {
				newList = new NodoHash<Key,Value>(current.getLlave(), current.getValor(), newList);
			}
		}

		return newList;
	}
	
	
	public void delete(Key key) {
		if(first == null) return;
		
		while(first != null && first.getLlave().equals(key)) {
			first = first.getSiguiente();
		}
		
		for(NodoHash<Key, Value> current = first; current != null && current.getSiguiente() != null; current = current.getSiguiente()) {
			if(current.getSiguiente().getLlave().equals(key)) {
				current.setSiguiente(current.getSiguiente().getSiguiente());
			}
		}		
	}
}