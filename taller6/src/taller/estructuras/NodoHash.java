package taller.estructuras;

public class NodoHash<K,V> 
{
	private K llave;
	private V valor;
	private NodoHash<K,V> sig;

	public NodoHash(K llave, V valor, NodoHash<K,V> siguiente) 
	{
		super();
		this.llave = llave;
		this.valor = valor;
		this.sig = siguiente;	
	}

	public K getLlave()
	{
		return llave;
	}

	public void setLlave(K llave)
	{
		this.llave = llave;
	}

	public V getValor() 
	{
		return valor;
	}

	public void setValor(V valor)
	{
		this.valor = valor;
	}
	
	public NodoHash<K,V> getSiguiente() 
	{
		return sig;
	}
	
	public void setSiguiente(NodoHash<K,V> siguiente) 
	{
		this.sig = siguiente;
	}
}